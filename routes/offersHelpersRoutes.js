const express = require('express')
const { restart } = require('nodemon')
const router = express.Router()
const OfferHelper =  require('../models/offersHelpers')
const User =  require('../models/users')
module.exports = router


router.get('/', async (req, res) => {
    try {
        const offerHelper = await OfferHelper.find()
        res.json(offerHelper)
    }
    catch (err) {
       res.status(500).res.json({message:err.message})
    }
})


router.post('/create_offerHelper', async (req, res) => {
    const offerHelper = new OfferHelper({
        /*  - type
            - city
            - maxNumber
            - period
            - description
            - idHelper
            - status*/

        type: req.body.type,
        city: req.body.city,
        maxNumber: req.body.maxNumber,
        period: req.body.period,
        description: req.body.description,
        idHelper: req.body.idHelper,
        status: req.body.status,
        helperContactInfo: req.body.helperContactInfo,
        contactInfo: "-",
    })

    try {
        const newOfferHelper = await offerHelper.save()
        res.status(201).json(newOfferHelper)
    } catch (err) {
        res.status(400).json({message:err.message})
    }
})

router.patch('/:email', getUser, async (req, res) => {
    //console.log(req.body)
    if (req.body.name != null) {
        //console.log("in req")
        res.user.name = req.body.name
        res.user.age = req.body.age
        res.user.place = req.body.place
        res.user.phone = req.body.phone
    }
    try {
        const updatedUser = await res.user.save()
        res.json(updatedUser)
    } catch (err) {
        res.status(400).json({message: err.message})
    }
})


router.patch('/updateContactInfo/:_id', getOffer, async (req, res) => {
    //console.log(res.offer)
    res.offer.refugeeContactInfo = req.body.refugeeContactInfo
    try {
        const updatedOffer = await res.offer.save()
        res.json(updatedOffer)
        //console.log(updatedOffer)
    } catch (err) {
        res.status(400).json({message: err.message})
    }
})

router.patch('/updateStatus/:_id', getOffer, async (req, res) => {
    //console.log(res.offer)
    res.offer.status = req.body.status
    try {
        //console.log("aicisa")
        const updatedOffer = await res.offer.save()
        res.json(updatedOffer)
    } catch (err) {
        res.status(400).json({message: err.message})
    }
})

async function getOffer(req,res,next) {
    //console.log("lmao")
    //console.log("aidi: " + req.params._id)
    let offer
    try {
        offer = await OfferHelper.find({"_id" : req.params._id})
        if (offer == null) {
            return res.status(404)
        }
    }
    catch (err) {
       res.status(500).json({message:err.message})
    }

    //console.log("ofar: " + offer)
    res.offer = offer[0]
    next()
}


async function getUser(req,res,next) {
    let user
    try {
        user = await User.find({"email" : req.params.email})
        if (user == null) {
            return res.status(404)
        }
    }
    catch (err) {
       res.status(500).json({message:err.message})
    }

    res.user = user[0]
    next()
}