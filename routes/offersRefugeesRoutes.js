const express = require('express')
const { restart } = require('nodemon')
const router = express.Router()
const OfferRefugee =  require('../models/offersRefugees')
const OfferHelper =  require('../models/offersHelpers')
const User =  require('../models/users')

module.exports = router



router.get('/getAllOffersHelper/:email',getUser, async (req, res) => {
    try {
        //console.log(res.user._id)
        const offersRefugee = await OfferRefugee.find({"idRefugee" : res.user._id})
        //console.log(offersRefugee)

        let idsList = offersRefugee.map((OfferRefugee) => OfferRefugee.idOfferHelper)
        //console.log(idsList)
        const offersHelperForRefugee = await OfferHelper.find({"_id" : idsList})
        offersHelperForRefugee.map((OfferHelper) => {if(OfferHelper.status === "created") OfferHelper.status = "declined" })
        //console.log(offersHelperForRefugee)
        res.json(offersHelperForRefugee)
    }
    catch (err) {
       res.status(500).res.json({message:err.message})
    }
})

router.patch('/:email', getUser, async (req, res) => {
    //console.log(req.body)
    if (req.body.name != null) {
        res.user.name = req.body.name
        res.user.age = req.body.age
        res.user.place = req.body.place
        res.user.phone = req.body.phone
    }
    try {
        const updatedUser = await res.user.save()
        //console.log(updatedUser)
        res.json(updatedUser)
    } catch (err) {
        res.status(400).json({message: err.message})
    }
})



router.post('/create_offerRefugee', async (req, res) => {
    const offerRefugee = new OfferRefugee({
        /* - id
        - idOfferHelper
        - status
        - idRefugee*/

        idOfferHelper: req.body.idOfferHelper,
        status: req.body.status,
        idRefugee: req.body.idRefugee
    })

    try {
        const newOfferRefugee = await offerRefugee.save()
        res.status(201).json(newOfferRefugee)
    } catch (err) {
        res.status(400).json({message:err.message})
    }
})

router.patch('/updateStatus/:_id', getOffer, async (req, res) => {
    //console.log(res.offer)
    res.offer.status = req.body.status
    try {
        //console.log("aicisa")
        const updatedOffer = await res.offer.save()
        res.json(updatedOffer)
    } catch (err) {
        res.status(400).json({message: err.message})
    }
})

async function getOffer(req,res,next) {
    //console.log("lmao")
    //console.log("aidi: " + req.params._id)
    let offer
    try {
        offer = await OfferRefugee.find({"idOfferHelper" : req.params._id})
        if (offer == null) {
            return res.status(404)
        }
    }
    catch (err) {
       res.status(500).json({message:err.message})
    }

    //console.log("ofar: " + offer)
    res.offer = offer[0]
    next()
}

async function getUser(req,res,next) {
    let user
    try {
        user = await User.find({"email" : req.params.email})
        if (user == null) {
            return res.status(404)
        }
    }
    catch (err) {
       res.status(500).json({message:err.message})
    }

    res.user = user[0]
    next()
}
