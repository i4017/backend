const express = require('express');
const { request } = require('http');
const router = express.Router()
module.exports = router

router.post('/sendMail/:recipientEmail/:type', async (req, res) => {
    const amqp = require('amqplib/callback_api');
    amqp.connect('amqp://rabbitmq:5672', function(error, connection) {
      if (error) {
        throw error;
      }
      connection.createChannel(function(error1, channel) {
        if (error1) {
          throw error1;
        }
    
        let queue = 'node_queue';
        let msg = req.params.recipientEmail + " " + req.params.type;
    
        channel.assertQueue(queue, {
          durable: true
        });
        channel.sendToQueue(queue, Buffer.from(msg), {
          persistent: true
        });
        console.log("Sent '%s'", msg);
      });
      
    });  
})