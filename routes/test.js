const express = require('express')
const { restart } = require('nodemon')
const router = express.Router()
const User =  require('../models/users')
module.exports = router

//Getting all
router.get('/', async (req, res) => {
    try {
        const users = await User.find()
        res.json(users)
    }
    catch (err) {
       res.status(500).res.json({message:err.message})
    }
})

router.get('/:email', getUser, (req, res) => {
    res.json(res.user.accountType)
})

router.get('/user/:email', getUser, (req, res) => {
    res.json(res.user)
})

router.get('/getID/:email', getUser, (req, res) => {
    res.json(res.user._id)
})

//Creating one
router.post('/create_user', async (req, res) => {
    const user = new User({
        accountType: req.body.accountType,
        name: req.body.name,
        age: req.body.age,
        place: req.body.place,
        phone: req.body.phone,
        email: req.body.email
        
    })
    try {
        const newUser = await user.save()
        res.status(201).json(newUser)
    } catch (err) {
        res.status(400).json({message:err.message})
    }
})

//Updating one
router.patch('/:email', getUser, async (req, res) => {
    if (req.body.accountType != null) {
        res.user.accountType = req.body.accountType
    }
    try {
        const updatedUser = await res.user.save()
        res.json(updatedUser)
    } catch (err) {
        res.status(400).json({message: err.message})
    }
})

//Deleting One
router.delete('/:email', (req, res) => {

})


async function getUser(req,res,next) {
    let user
    try {
        user = await User.find({"email" : req.params.email})
        if (user == null) {
            return res.status(404)
        }
    }
    catch (err) {
       res.status(500).json({message:err.message})
    }

    res.user = user[0]
    next()
}