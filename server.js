require('dotenv').config()

const express = require('express')
const app = express()
const mongoose = require('mongoose')

mongoose.connect('mongodb://mongo:27017/backend', { useNewUrlParser: true }
).then(() => console.log('MongoDB Connected')).catch(err => 
    {
        console.log("de ceeeee?");
        console.log(err)});
const db = mongoose.connection
db.on('error', (error) => console.error(error))
db.once('open', () => console.log('Connected to Database'))

app.use(express.json())

app.get("/api",(req,res) => {
    res.json({"users":[{"name": "user1","activated" : false},{"name": "user2","activated" : true},{"name": "user3","activated" : false}
            ,{"name": "user4","activated" : true},{"name": "gigel","activated" : false}]})
})

const testRouter = require('./routes/test')
app.use('/test', testRouter)

const emailRouter = require('./routes/emailNotification')
app.use('/emailNotification', emailRouter)

const offerRefugeesRouter = require('./routes/offersRefugeesRoutes')
app.use('/offerRefugeesRouter', offerRefugeesRouter)

const offerHelpersRouter = require('./routes/offersHelpersRoutes')
app.use('/offerHelpersRouter', offerHelpersRouter)

app.listen(5000,() => {console.log("Server started on port 5000")})