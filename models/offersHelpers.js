const mongoose = require('mongoose')

const offersHelpersSchema = new mongoose.Schema({

   /* - type
    - city
    - maxNumber
    - period
    - description
    - idHelper
    - status*/

    type:{
        type: String,
        required: true
    },
    city:{
        type: String,
        required: true
    },
    maxNumber:{
        type: String,
        required: true
    },
    period: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    idHelper: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    helperContactInfo: {
        type: String
    },
    refugeeContactInfo: {
        type: String
    }

})

module.exports = mongoose.model('OfferHelper',offersHelpersSchema)