const mongoose = require('mongoose')

const offersRefugeesSchema = new mongoose.Schema({

   /* - id
      - idOfferHelper
      - status
      - idRefugee*/

    idOfferHelper:{
        type: String,
        required: true
    },
    status:{
        type: String,
        required: true
    },
    idRefugee:{
        type: String,
        required: true
    }

})

module.exports = mongoose.model('OfferRefugee',offersRefugeesSchema)