const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({

    /*tabela Users
      - id
      - accountType
      - name
      - age
      - place
      - phone
      - email*/


    accountType:{
        type: String,
        required: true
    },
    name:{
        type: String,
        required: true
    },
    age:{
        type: String,
        required: true
    },
    place:{
        type: String,
        required: true
    },
    phone:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true
    },
   
})

module.exports = mongoose.model('User',userSchema)